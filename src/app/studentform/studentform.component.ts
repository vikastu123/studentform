import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-studentform',
  templateUrl: './studentform.component.html',
  styleUrls: ['./studentform.component.css']
})
export class StudentformComponent implements OnInit {

  constructor(
    private http : HttpClient
  ) { }

  tdata = []

  ngOnInit(): void {
    this.getstudentform()
  }

  updatebtn : any = false
  updateid = ''

  studentname : any = ''
  password : any = ''

  submitform(){
    let form = {
      studentname : this.studentname,
      password  : this.password
    }
    this.http.post('http://localhost:3000/register',form).subscribe(()=>{
      this.getstudentform()

    },(err)=>{
      console.log(err)
    })
  }

  updateform(){
    let form = {
      _id : this.updateid,
      studentname : this.studentname,
      password  : this.password
    }
    this.http.post('http://localhost:3000/update',form).subscribe(()=>{
      this.getstudentform()

    },(err)=>{
      console.log(err)
    })
  }


  getstudentform(){
    // studentlist
    this.http.get('http://localhost:3000/studentlist').subscribe((data:any)=>{
          this.tdata = data
    },(err)=>{
      console.log(err)
    })
  }

  delete(data:any){
    this.http.post('http://localhost:3000/delete',data).subscribe((data:any)=>{
      this.tdata = data
},(err)=>{
  console.log(err)
})
  }


  update(data:any){
    console.log(data)
    this.studentname = data.studentname
    this.password = data.password
    this.updateid = data._id
    this.updatebtn = true
    
  }


}
