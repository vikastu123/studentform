const express = require('express')
const cors = require('cors')
const app = express()
const bodyparser = require('body-parser')
// const mongoose = require('mongoose')
const mongo = require('mongodb').MongoClient
// const studentform = require('./modal/studentform')

app.use(cors())
app.use(bodyparser.json())



app.listen(3000, () => {
    console.log('server is running in 3000')
    mongo.connect('mongodb://localhost:27017/', (err, db) => {
        if (err) {
            console.log(err)
        }
        console.log('connected')
    })
})



app.post('/register', (req, res) => {
    try {
        console.log(req.body)
        mongo.connect('mongodb://localhost:27017/', (err, db) => {
            if (err) {
                console.log(err)
            }
            const dbo = db.db("studentform")
            dbo.collection('studentform').insertOne(req.body, (err, res) => {
                if (err) {
                    console.log(err)
                }
            })
        })
        res.send('recived')

    } catch (err) {
        console.log(err)
    }
})


app.post('/update', (req, res) => {
    try {
        console.log(req.body)

        let newdata = req.body
        delete newdata['_id']
        mongo.connect('mongodb://localhost:27017/', (err, db) => {
            if (err) {
                console.log(err)
            }
            const dbo = db.db("studentform")
            // dbo.collection('studentform').updateOne({_id:req.body['_id']},{$set : newdata} , (err, res) => {
            //     if (err) {
            //         console.log(err)
            //     }
            //     console.log(res)
            // })
            dbo.collection('studentform').updateOne({studentname:req.body['studentname']},{$set : newdata} , (err, res) => {
                if (err) {
                    console.log(err)
                }
                console.log(res)
            })
        })
        res.send('recived')

    } catch (err) {
        console.log(err)
    }
})


app.get('/studentlist',(req,res)=>{
    try {
        console.log(req.body)
        mongo.connect('mongodb://localhost:27017/', (err, db) => {
            if (err) {
                console.log(err)
            }
            console.log('connected')
            const dbo = db.db("studentform")
            dbo.collection('studentform').find({},{}).toArray((err,data)=>{
                if(err)
                {
                    console.log(err)
                }
                res.json(data)
            })
        })

    } catch (err) {
        console.log(err)
    }
})


app.post('/delete', (req, res) => {

    try {
     
        delete req.body['_id']
        console.log(req.body)
        mongo.connect('mongodb://localhost:27017/', (err, db) => {
            if (err) {
                console.log(err)
            }
            const dbo = db.db("studentform")
            dbo.collection('studentform').deleteOne(req.body, (err, res) => {
                if (err) {
                    console.log(err)
                }
                console.log(res)
            })
        })
        res.send('recived')

    } catch (err) {
        console.log(err)
    }

})